import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)

import os
import face_recognition
import cv2
import matplotlib.pyplot as plt
import glob
from tqdm import tqdm
# img = cv2.imread("images/images_100010669801019/images/74531605_2645454668855574_3452616276726251520_o_.png")
#
# vframe = cv2.cvtColor(img,cv2.COLOR_BGR2RGB)
# # plt.imshow(vframe)
# # plt.show()
# face_positions = face_recognition.face_locations(vframe)
# i=0
# for face_position in face_positions:
#     y0,x1,y1,x0 = face_position
#     i+=1
#     # img = cv2.rectangle(vframe,(x0,y0),(x1,y1),(255,0,0),5)
#     plt.subplot(len(face_positions),1,i)
#     plt.imshow(vframe[y0:y1,x0:x1])
#
# # plt.imshow(img)
# plt.show()

margin = 0.1
img_folders = glob.glob("./images/*/")
for img_folder in tqdm(img_folders):
    img_paths = glob.glob(img_folder+"/images/*.png")
    if not os.path.exists(img_folder+"/images_face_detection"):
        os.mkdir(img_folder+"/images_face_detection")
    for img_path in img_paths:
        img = cv2.imread(img_path)
        vframe = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        face_positions = face_recognition.face_locations(vframe)
        for i,face_position in enumerate(face_positions):
            offset = round(margin * (face_position[2] - face_position[0]))
            y0 = max(face_position[0] - offset, 0)
            x1 = min(face_position[1] + offset, img.shape[1])
            y1 = min(face_position[2] + offset, img.shape[0])
            x0 = max(face_position[3] - offset, 0)
            face = vframe[y0:y1, x0:x1]
            plt.imsave(img_folder + "/images_face_detection/" + img_path.split("/")[-1].split(".")[0] +"_" +str(i)+".jpg",face)

# print(img_file)
