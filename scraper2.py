import argparse
import time
import json

from selenium import webdriver
import pickle


driver = webdriver.Chrome('./chromedriver', )

driver.get("http://facebook.com")

def load_cookie(path):
    with open(path, 'rb') as cookiesfile:
        cookies = pickle.load(cookiesfile)
        for cookie in cookies:
            if 'expiry' in cookie:
                del cookie['expiry']
            driver.add_cookie(cookie)


def login():
    email = driver.find_element_by_name("email")
    email.send_keys("minhtuanhust75")
    pwd = driver.find_element_by_name("pass")
    pwd.send_keys('123!@#')
    pwd.submit()

    facebook_cookies = driver.get_cookies()
    with open("cookies.pkl", "wb") as fd:
        pickle.dump(facebook_cookies, fd)

def get_image():
    pass

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Facebook Page Scraper")
    required_parser = parser.add_argument_group("required arguments")
    required_parser.add_argument('-page', '-p', help="The Facebook Public Page you want to scrape", required=True)
    required_parser.add_argument('-len', '-l', help="Number of Posts you want to scrape", type=int, required=True)
    optional_parser = parser.add_argument_group("optional arguments")
    optional_parser.add_argument('-infinite', '-i',
                                 help="Scroll until the end of the page (1 = infinite) (Default is 0)", type=int,
                                 default=0)
    args = parser.parse_args()

    infinite = False
    if args.infinite == 1:
        infinite = True

    scrape_comment = False
    if args.comments == 'y':
        scrape_comment = True
    click = args.click
    print(click)
    postBigDict = extract(page=args.page, numOfPost=args.len, infinite_scroll=infinite, scrape_comment=scrape_comment,click = click)
    # postBigDict
    if args.usage == "WT":
        with open('output_moon.json', 'w', encoding='utf-8') as file:
            data  = json.dumps(postBigDict, ensure_ascii=False)
            file.write(data)
            # for post in postBigDict:
            #     file.write((json.dumps(post, ensure_ascii=False)))  # use json load to recover
    else:
        for post in postBigDict:
            print(post)
            print("\n")

    print("Finished")
# python scraper2.py -p moonbyun1 -l 1 -c y -u WT -cl 7

