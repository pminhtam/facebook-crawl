from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException, WebDriverException
import sys, os, re, json
from datetime import datetime
import pickle
from selenium.webdriver.chrome.options import Options
from PIL import Image
from io import BytesIO
import numpy as np
import time
import argparse


# sys.path.insert(0,os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
# from neo4jDir.createneo4j import ImportNeo4j


class Livestream:
    LOGIN_URL = 'https://www.facebook.com/login.php?login_attempt=1&lwv=111'

    def __init__(self, login, password):
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("user-data-dir=./chrome")

        prefs = {"profile.default_content_setting_values.notifications": 2}
        chrome_options.add_experimental_option("prefs", prefs)
        chrome_options.add_argument("headless")
        self.driver = webdriver.Chrome(executable_path=r'./chromedriver', chrome_options=chrome_options)

        self.wait = WebDriverWait(self.driver, 10)
        self.driver.get('https://www.facebook.com')
        # self.login(login, password)
        # self.load_cookie('cookies.pkl')
        if os.path.exists('cookies.pkl'):
            self.load_cookie('cookies.pkl')
        else:
            self.login(login, password)

    def login(self, login, password):
        self.driver.get(self.LOGIN_URL)

        # wait for the login page to load
        self.wait.until(EC.visibility_of_element_located((By.ID, "email")))

        self.driver.find_element_by_id('email').send_keys(login)
        self.driver.find_element_by_id('pass').send_keys(password)
        self.driver.find_element_by_id('loginbutton').click()

        # wait for the main page to load
        self.wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, "div[data-click='profile_icon']")))

        facebook_cookies = self.driver.get_cookies()
        print(facebook_cookies)
        with open("cookies.pkl", "wb") as fd:
            pickle.dump(facebook_cookies, fd)

        # self.driver.quit()

        # return cookies

    def load_cookie(self, path):
        with open(path, 'rb') as cookiesfile:
            cookies = pickle.load(cookiesfile)
            for cookie in cookies:
                if 'expiry' in cookie:
                    del cookie['expiry']
                self.driver.add_cookie(cookie)

    def _get_amount_image_list(self, select_elem):
        self.wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, select_elem)))
        return self.driver.find_elements_by_css_selector(select_elem)

    def crawl_get_list_image_tagged(self, fb_id):
        # navigate to "image tagged" page
        try:
            self.driver.get("https://www.facebook.com/%s/photos_of" % fb_id)

            # continuous scroll until no more new friends loaded
            num_of_loaded_images = len(self._get_amount_image_list("li.fbPhotoStarGridElement"))

            while True:
                self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                try:
                    self.wait.until(
                        lambda driver: len(
                            self._get_amount_image_list("li.fbPhotoStarGridElement")) > num_of_loaded_images)
                    num_of_loaded_images = len(self._get_amount_image_list("li.fbPhotoStarGridElement"))
                except TimeoutException:
                    break  # no more friends loaded
            return [image for image in self._get_amount_image_list("li.fbPhotoStarGridElement")]
        except TimeoutException:
            print('Time out!')
            return

    def crawl_get_image_tagged(self, fb_id):
        list_all_images = self.crawl_get_list_image_tagged(fb_id)

        if list_all_images is not None:
            ts = time.time()
            time_allow = 150076800  # Five year
            for image in list_all_images:
                image_fb_id = image.get_attribute('data-fbid')
                href = str(image.find_element_by_xpath('.//a').get_attribute('href'))
                if 'videos' in href.split('/'):
                    continue
                try:
                    # list_current_images_id.append(image_fb_id)
                    image.click()
                    self.wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, '.photoTagLink')))

                    img_time = self.driver.find_element_by_id(
                        'fbPhotoSnowliftTimestampAudienceContainer').find_element_by_css_selector('abbr').get_attribute(
                        'data-utime')
                    if img_time is not None and int(img_time) - ts > time_allow:
                        continue

                    list_tag = self.driver.find_elements_by_css_selector('.photoTagLink')
                    for tag in list_tag:
                        fb_id = tag.find_element_by_css_selector('div.fbPhotosPhotoTagboxBase').get_attribute('id')
                        fb_id = re.match(r'.*tag:(.*)$', fb_id).group(1)

                        element = tag.find_element_by_css_selector(
                            'div.fbPhotosPhotoTagboxBase .borderTagBox').screenshot_as_png
                        im = Image.open(BytesIO(element))  # uses PIL library to open image in memory
                        im.save(os.path.join('images', '%s_%s.png' % (fb_id, image_fb_id)))
                    # When finish => Click "X" to close and continue click other images
                    self.driver.find_element_by_xpath('//*[@id="photos_snowlift"]/div[2]/div/a/i/u').click()
                except WebDriverException:
                    continue

    def crawl_get_image(self, fb_id):
        json_data = {}
        id_friend = []
        if not os.path.exists("./images/images_"+str(fb_id)):
            os.mkdir("./images/images_"+str(fb_id))
        if not os.path.exists("./images/images_"+str(fb_id)+ '/images'):
            os.mkdir("./images/images_"+str(fb_id)+ '/images')
        if not os.path.exists("./images/images_"+str(fb_id)+ '/images_crop'):
            os.mkdir("./images/images_"+str(fb_id)+ '/images_crop')
        try:
            self.driver.get("https://www.facebook.com/%s/photos_of" % fb_id)
            # self.driver.get("https://www.facebook.com/pg/%s/photos" % fb_id)

            # continuous scroll until no more new friends loaded
            # num_of_loaded_images = len(self._get_amount_image_list("li.fbPhotoStarGridElement"))
            num_of_loaded_images = 1000

            while True:
                self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
                try:
                    self.wait.until(
                        lambda driver: len(
                            self._get_amount_image_list("li.fbPhotoStarGridElement")) > num_of_loaded_images)
                    num_of_loaded_images = len(self._get_amount_image_list("li.fbPhotoStarGridElement"))
                except TimeoutException:
                    break  # no more friends loaded
            ts = time.time()
            time_allow = 150076800  # Five year

            for image in self._get_amount_image_list("li.fbPhotoStarGridElement"):
                # print(image)
                image_name = image.get_attribute('data-starred-src').split("/")[-1].split('?')[0].split('.')[0] # id of user
                # if os.path.exists(os.path.join("./images_"+str(fb_id) + '/images', '%s_.png' % (image_name))):
                #     continue
                href = str(image.find_element_by_xpath('.//a').get_attribute('href'))

                if 'videos' in href.split('/'):
                    continue
                try:
                    # list_current_images_id.append(image_fb_id)
                    image.click()
                    self.wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, '.photoTagLink')))


                    # Save full image
                    # time.sleep(5)
                    # imgs = self.driver.find_elements_by_css_selector('div.fbPhotosPhotoTagboxes')
                    img = self.driver.find_element_by_xpath('//div[@class="tagsWrapper"]')
                    # print(img.get_attribute('style'))
                    element = img.screenshot_as_png
                    im = Image.open(BytesIO(element))  # uses PIL library to open image in memory
                    im.save(os.path.join("./images/images_"+str(fb_id) + '/images', '%s_.png' % (image_name)))

                    # Save image tag
                    img_time = self.driver.find_element_by_id(
                        'fbPhotoSnowliftTimestampAudienceContainer').find_element_by_css_selector('abbr').get_attribute(
                        'data-utime')
                    if img_time is not None and int(img_time) - ts > time_allow:
                        continue
                    json_data[image_name] = []
                    list_tag = self.driver.find_elements_by_css_selector('.photoTagLink')
                    for tag in list_tag:

                        tag_element = tag.find_element_by_css_selector('div.fbPhotosPhotoTagboxBase')

                        tag_style = tag_element.get_attribute('style')
                        friend_fb_id = tag_element.get_attribute('id')
                        friend_fb_id = re.match(r'.*tag:(.*)$', friend_fb_id).group(1)
                        id_friend.append(friend_fb_id)
                        # id_friend.append(fb_id)
                        json_data[image_name].append({friend_fb_id:tag_style})
                        element = tag.find_element_by_css_selector(
                            'div.fbPhotosPhotoTagboxBase .borderTagBox').screenshot_as_png
                        im = Image.open(BytesIO(element))  # uses PIL library to open image in memory
                        im.save(os.path.join("./images/images_"+str(fb_id) + '/images_crop', '%s_%s.png' % (image_name, friend_fb_id)))


                    self.driver.find_element_by_xpath('//*[@id="photos_snowlift"]/div[2]/div/a').click()
                    # print(json_data)
                except WebDriverException:
                    print(' 209 WebDriverException! ')
                    continue
            # json.dump(json_data,'data.json')
        except TimeoutException:
            print('213  Time out!')
            return json_data,id_friend
        return json_data,id_friend

    def get_friend_list(self, fb_id):
        self.driver.get('https://fb.com/%s/friends' % fb_id)
        num_of_loaded_images = len(self._get_amount_image_list("li._698"))
        while True:
            self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
            try:
                self.wait.until(lambda driver: len(self._get_amount_image_list("li._698")) > num_of_loaded_images)
                num_of_loaded_images = len(self._get_amount_image_list("li._698"))
            except TimeoutException:
                break
        friend_elements = self.driver.find_elements_by_class_name("_698")
        friend_list = []
        for elem in friend_elements:
            friend = elem.find_element_by_xpath(".//div/a")
            friend_link = friend.get_attribute('href')
            fb_id = str(friend_link)[25:-32].replace('profile.php?id=', '')
            if fb_id == '' or fb_id is None:
                continue
            friend_list.append(fb_id)
        with open('friend_list.pkl', 'wb') as f:
            pickle.dump(friend_list, f)
        return friend_list


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='facebook image crawler')
    # general
    parser.add_argument('--fb_ids', default='fb_ids.txt', help='path to load list facebook id.')
    parser.add_argument('--craw_from_file', type=bool, default=True, help='crawl from a list of fb ids or from friend '
                                                                           'list of an account')
    args = parser.parse_args()

    crawler = Livestream(login="minhtuanhust75", password="123!@#")

    if args.craw_from_file:
        assert os.path.exists(args.fb_ids), '%s is not exists'%args.fb_ids
        with open('fb_ids.txt', 'r') as f:
            while True:
                id = f.readline().replace('\n', '')
                if id is None or id == '':
                    break
                json_data = {}
                try:
                    # crawler.crawl_get_image_tagged(id)
                    # 100003868856135
                    # dang1313
                    json_data,id_friend = crawler.crawl_get_image(id)
                    json.dump(json_data, open('data_'+ str(id)+'.json','w'))
                except Exception as e:
                    print(e)
                    json.dump(json_data, open('data_'+ str(id)+'.json','w+'))
                    continue
                # get image of friend of people
                for id in id_friend:
                    try:
                        # crawler.crawl_get_image_tagged(id)
                        # 100003868856135
                        # dang1313
                        json_data, id_friend = crawler.crawl_get_image(id)
                        json.dump(json_data, open('data_' + str(id) + '.json', 'w+'))
                    except Exception as e:
                        print(e)
                        json.dump(json_data, open('data_' + str(id) + '.json', 'w+'))
                        continue
    else:
        friend_list_fb_id = crawler.get_friend_list('minhtuanhust75')
        if friend_list_fb_id is not None and len(friend_list_fb_id) > 0:
            for fb_id in friend_list_fb_id:
                crawler.crawl_get_image_tagged(fb_id)